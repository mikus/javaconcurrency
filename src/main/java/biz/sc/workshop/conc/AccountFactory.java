package biz.sc.workshop.conc;

public interface AccountFactory {

    Account createAccount(final int amount);

    AccountService createService();

    void close();
}
