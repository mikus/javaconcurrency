package biz.sc.workshop.conc.example3a;

import akka.actor.UntypedActor;

public class StuntPerson extends UntypedActor {

    @Override
    public void onReceive(final Object stunt) throws Exception {
        System.out.println(hashCode() + " doing ... " + stunt);
    }
}
