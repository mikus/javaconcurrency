package biz.sc.workshop.conc.example2b;

import biz.sc.workshop.conc.Account;
import biz.sc.workshop.conc.AccountFactory;
import biz.sc.workshop.conc.AccountService;

public class AccountFactoryImpl implements AccountFactory {

    @Override
    public Account createAccount(int amount) {
        return new AccountImpl(amount);
    }

    @Override
    public AccountService createService() {
        return new AccountServiceImpl();
    }

    @Override
    public void close() {

    }
}
