package biz.sc.workshop.conc.example2b;

import biz.sc.workshop.conc.Account;
import biz.sc.workshop.conc.AccountService;
import clojure.lang.LockingTransaction;

public class AccountServiceImpl implements AccountService {

    public void transfer(final Account from, final Account to, final int amount) throws Exception {
        LockingTransaction.runInTransaction(() -> {
            to.deposit(amount);
            from.withdraw(amount);
            return null;
        });
    }
}
