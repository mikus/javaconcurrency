package biz.sc.workshop.conc.example2b;

import biz.sc.workshop.conc.Account;
import clojure.lang.LockingTransaction;
import clojure.lang.Ref;

public class AccountImpl implements Account {
    private Ref balance;

    public AccountImpl(int initialBalance) { balance = new Ref(initialBalance); }

    public void deposit(final int amount) throws Exception {
        if (amount < 0) throw new RuntimeException("Invalid amount");

        LockingTransaction.runInTransaction(() -> {
            balance.set(getBalance() + amount);
            return null;
        });
    }

    public void withdraw(final int amount) throws Exception {
        if (amount < 0)
            throw new RuntimeException("Invalid amount");
        LockingTransaction.runInTransaction(() -> {
            if (amount > getBalance())
                throw new RuntimeException("Insufficient fund");
            balance.set(getBalance() - amount);
            return null;
        });
    }

    public int getBalance() { return (Integer) balance.deref(); }
}