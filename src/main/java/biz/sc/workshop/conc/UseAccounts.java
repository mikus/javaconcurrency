package biz.sc.workshop.conc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class UseAccounts {

    private static AccountFactory factory = new biz.sc.workshop.conc.example0.AccountFactoryImpl();

    public static void main(String[] args) throws Exception {
        final AccountService service = factory.createService();
        int times = 5;

        final Account account1A = factory.createAccount(1000);
        final Account account2A = factory.createAccount(1000);

        runSynchronously(() -> {
            service.transfer(account1A, account2A, 500);
            service.transfer(account2A, account1A, 300);
            return null;
        }, times);

        Thread.sleep(1000);

        printBalances(account1A, account2A);

        final Account account1B = factory.createAccount(1000);
        final Account account2B = factory.createAccount(1000);

        runConcurrently(() -> {
            service.transfer(account1B, account2B, 500);
            service.transfer(account2B, account1B, 300);
            return null;
        }, times);

        Thread.sleep(1000);

        printBalances(account1B, account2B);

        factory.close();
    }

    private static void printBalances(Account... accounts) {
        for (Account account : accounts) {
            try {
                System.out.println(account.getBalance());
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
        System.out.println();
    }

    private static void runSynchronously(Callable<Void> program, int times) {
        for (int i = 0; i < times; ++i)
            try {
                program.call();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
    }

    private static void runConcurrently(Callable<Void> program, int threads) {
        ExecutorService executor = Executors.newFixedThreadPool(threads);
        List<Callable<Void>> programs = new ArrayList<>();
        for (int i = 0; i < threads; ++i)
            programs.add(program);
        try {
            List<Future<Void>> futures = executor.invokeAll(programs);
            for (Future<Void> future : futures) {
                try {
                    future.get();
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }
}
