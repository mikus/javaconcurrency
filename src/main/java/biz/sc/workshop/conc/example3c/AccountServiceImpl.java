package biz.sc.workshop.conc.example3c;

import akka.actor.*;
import akka.transactor.SendTo;
import akka.transactor.UntypedTransactor;
import biz.sc.workshop.conc.Account;
import biz.sc.workshop.conc.AccountService;
import scala.concurrent.duration.Duration;

import java.util.HashSet;
import java.util.Set;

import static akka.actor.SupervisorStrategy.stop;

public class AccountServiceImpl implements AccountService {

    private ActorRef actor;

    AccountServiceImpl(ActorSystem actorSystem) {
        actor = actorSystem.actorOf(Props.create(AccountServiceActor.class));
    }

    @Override
    public void transfer(Account from, Account to, int amount) throws Exception {
        transfer(((AccountImpl)from).getActor(), ((AccountImpl)to).getActor(), amount);
    }

    private void transfer(ActorRef from, ActorRef to, int amount) throws Exception {
        actor.tell(new TransferMessage(from, to, amount), null);
    }
}

class AccountServiceActor extends UntypedTransactor {

    @Override
    public Set<SendTo> coordinate(Object message) {
        if (message instanceof TransferMessage) {
            TransferMessage transfer = (TransferMessage) message;
            Set<SendTo> participants = new HashSet<>();
            participants.add(sendTo(transfer.getFrom(), new WithdrawMessage(transfer.getAmount())));
            participants.add(sendTo(transfer.getTo(), new DepositMessage(transfer.getAmount())));
            return participants;
        }
        return nobody();
    }

    @Override
    public void atomically(Object message) throws Exception {
        /*if (message instanceof TransferMessage) {
            TransferMessage transfer = (TransferMessage) message;
            transfer.getFrom().tell(new WithdrawMessage(transfer.getAmount()), self());
            transfer.getTo().tell(new DepositMessage(transfer.getAmount()), self());
        }*/
    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return new AllForOneStrategy(10, Duration.create("10 second"),
                t -> {
                    System.out.println("DUPA");
                    return stop();
                });
    }
}

class TransferMessage {

    private ActorRef from;
    private ActorRef to;
    private int amount;

    TransferMessage(ActorRef from, ActorRef to, int amount) {
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    ActorRef getFrom() { return from; }

    ActorRef getTo() { return to; }

    int getAmount() { return amount; }

}