package biz.sc.workshop.conc.example3c;

import akka.actor.ActorSystem;
import biz.sc.workshop.conc.Account;
import biz.sc.workshop.conc.AccountFactory;
import biz.sc.workshop.conc.AccountService;

public class AccountFactoryImpl implements AccountFactory {

    private ActorSystem actorSystem;

    public AccountFactoryImpl() {
        actorSystem = ActorSystem.create();
    }

    @Override
    public Account createAccount(int amount) {
        return new AccountImpl(actorSystem, amount);
    }

    @Override
    public AccountService createService() {
        return new AccountServiceImpl(actorSystem);
    }

    @Override
    public void close() {
        actorSystem.shutdown();
    }
}
