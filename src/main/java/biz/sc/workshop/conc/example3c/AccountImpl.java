package biz.sc.workshop.conc.example3c;

import akka.actor.*;
import akka.transactor.UntypedTransactor;
import akka.util.Timeout;
import biz.sc.workshop.conc.Account;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import scala.concurrent.stm.Ref;
import scala.concurrent.stm.japi.STM;

import java.util.concurrent.TimeUnit;

import static akka.pattern.Patterns.ask;

public class AccountImpl implements Account {

    private ActorRef actor;
    private Ref.View<Integer> balance = STM.newRef(0);

    public AccountImpl(ActorSystem actorSystem, int initialBalance) {
        balance.set(initialBalance);
        actor = actorSystem.actorOf(Props.create(AccountActor.class, balance));
    }

    public void deposit(final int amount) {
        actor.tell(new DepositMessage(amount), null);
    }

    public void withdraw(final int amount) {
        actor.tell(new WithdrawMessage(amount), null);
    }

    public int getBalance() {
        try {
            Timeout timeout = new Timeout(Duration.create(10, TimeUnit.SECONDS));
            Future<Object> future = ask(actor, new BalanceMessage(), timeout);
            return ((BalanceMessage)Await.result(future, timeout.duration())).getValue();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    ActorRef getActor() {
        return actor;
    }
}

class AccountActor extends UntypedTransactor {

    private Ref.View<Integer> balance;

    AccountActor(Ref.View<Integer> balance) {
        this.balance = balance;
    }

    @Override
    public void atomically(Object message) throws Exception {
        if (message instanceof DepositMessage)
            deposit(((DepositMessage) message).getValue());
        else if (message instanceof WithdrawMessage)
            withdraw(((WithdrawMessage) message).getValue());
    }

    @Override
    public boolean normally(Object message) {
        if (message instanceof  BalanceMessage) {
            sender().tell(new BalanceMessage(getBalance()), self());
            return true;
        }
        return false;
    }

    private void deposit(int amount) {
        if (amount < 0)
            throw new RuntimeException("Invalid amount");
        balance.set(balance.get() + amount);
    }

    private void withdraw(int amount) {
        if (amount < 0)
            throw new RuntimeException("Invalid amount");
        if (amount > balance.get())
            throw new RuntimeException("Insufficient fund");
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        balance.set(balance.get() - amount);
    }

    private int getBalance() {
        return balance.get();
    }
}

class DepositMessage {
    private int value;

    DepositMessage(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

class WithdrawMessage {
    private int value;

    WithdrawMessage(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

class BalanceMessage {
    private int value;

    BalanceMessage() { }

    BalanceMessage(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}