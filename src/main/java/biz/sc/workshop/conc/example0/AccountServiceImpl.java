package biz.sc.workshop.conc.example0;

import biz.sc.workshop.conc.Account;
import biz.sc.workshop.conc.AccountService;

public class AccountServiceImpl implements AccountService {

    @Override
    public void transfer(Account from, Account to, int amount) throws Exception {
        from.withdraw(amount);
        to.deposit(amount);
    }
}