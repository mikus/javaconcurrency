package biz.sc.workshop.conc.example0;

import biz.sc.workshop.conc.Account;

public class AccountImpl implements Account {

    private int balance;

    public AccountImpl(final int initialBalance) {
        balance = initialBalance;
    }

    public void deposit(final int amount) throws InterruptedException {
        if (amount < 0)
            throw new RuntimeException("Invalid amount");
        Thread.sleep(100);
        balance += amount;
    }

    public void withdraw(final int amount) throws InterruptedException {
        if (amount < 0)
            throw new RuntimeException("Invalid amount");
        if (amount > getBalance())
            throw new RuntimeException("Insufficient fund: " + Thread.currentThread().getId());
        Thread.sleep(100);
        balance -= amount;
    }

    public int getBalance() { return balance; }
}
