package biz.sc.workshop.conc;

public interface AccountService {

    void transfer(final Account from, final Account to, final int amount) throws Exception;
}