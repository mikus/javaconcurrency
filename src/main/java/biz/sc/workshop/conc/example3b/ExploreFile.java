package biz.sc.workshop.conc.example3b;

import akka.actor.UntypedActor;

import java.io.File;

public class ExploreFile extends UntypedActor {
    @Override
    public void onReceive(final Object message) throws Exception {
        String path = (String) message;

        int count = 0;
        File file = new File(path);
        File[] list = file.listFiles();
        if (list != null) {
            for (File child : list) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (child.isFile())
                    count++;
                else
                    sender().tell(child.getPath(), self());
            }
        }

        sender().tell(count, self());
    }
}
