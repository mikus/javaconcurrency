package biz.sc.workshop.conc.example1b;

import biz.sc.workshop.conc.Account;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AccountImpl implements Account {

    private int balance;
    public final Lock monitor = new ReentrantLock();

    public AccountImpl(final int initialBalance) {
        balance = initialBalance;
    }

    public void deposit(final int amount) throws InterruptedException {
        if (amount < 0)
            throw new RuntimeException("Invalid amount");
        if (monitor.tryLock(1, TimeUnit.SECONDS)) {
            try {
                balance += amount;
            } finally {
                monitor.unlock();
            }
        } else {
            throw new RuntimeException("No luck");
        }
    }

    public void withdraw(final int amount) throws InterruptedException {
        if (amount < 0)
            throw new RuntimeException("Invalid amount");
        if (monitor.tryLock(1, TimeUnit.SECONDS)) {
            try {
                if (amount > getBalance())
                    throw new RuntimeException("Insufficient fund");
                Thread.sleep(100);
                balance -= amount;
            } finally {
                monitor.unlock();
            }
        } else {
            throw new RuntimeException("No luck");
        }
    }

    public int getBalance() throws InterruptedException {
        if (monitor.tryLock(1, TimeUnit.SECONDS)) {
            try {
                return balance;
            } finally {
                monitor.unlock();
            }
        } else {
            throw new RuntimeException("No luck");
        }
    }
}
