package biz.sc.workshop.conc.example1b;

import biz.sc.workshop.conc.*;

import java.util.concurrent.TimeUnit;

public class AccountServiceImpl implements AccountService {

    @Override
    public void transfer(final Account from, final Account to, int amount) throws Exception {
        realTransfer((AccountImpl)from, (AccountImpl)to, amount);
    }

    private void realTransfer(final AccountImpl from, final AccountImpl to, int amount) throws Exception {
        if (from.monitor.tryLock(1, TimeUnit.SECONDS)) {
            try {
                if (to.monitor.tryLock(1, TimeUnit.SECONDS)) {
                    try {
                        from.withdraw(amount);
                        to.deposit(amount);
                    } finally {
                        to.monitor.unlock();
                    }
                } else {
                    throw new RuntimeException("no lock to");
                }
            } finally {
                from.monitor.unlock();
            }
        } else {
            throw new RuntimeException("no lock from");
        }
    }
}