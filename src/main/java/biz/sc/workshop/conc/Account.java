package biz.sc.workshop.conc;

public interface Account {

    void deposit(final int amount) throws Exception;

    void withdraw(final int amount) throws Exception;

    int getBalance() throws Exception;
}
