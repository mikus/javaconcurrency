Concurrency models in Java
==========================

* example0 - naive (incorrect) implementation
* example1 - SSM (synchronize & suffer model)
* example2 - STM (software transactional memory)
* example3 - ABM (actor based model)

Disclaimer
----------

This workshop was prepared for my development team in [Algolytics](http://algolytics.com/).
Examples base on [warsjawa](http://warsjawa.pl) workshop made by [Venkat Subramaniam](https://twitter.com/venkat_s).

License
-------

All examples are under [Apache License v 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).